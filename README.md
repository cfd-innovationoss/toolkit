# cfd-db-adapter

Boîte à outils CFD-Innovation

## CFD_DB_ADAPTER

Classe permettant de switch entre une base de données DB2, MySQL ou tout autre connecteur supporté par PDO

### Prérequis

- DB2 : ext-ibm_db2
- MySQL : ext-mysqli
- PDO : ext-pdo

### Types de connecteurs disponibles

- mysql
- db2
- pdo
  * ODBC
  * SqlSrv
  * DBLib

### Exemples :

Utilisation de différents connecteurs :
```php
// MySQL
$db = FactoryAdapter::getInstance("mysql", "localhost", "user", "password", "dbname");
// DB2
$db = FactoryAdapter::getInstance("db2", "*LOCAL", "user", "password", "dbname", 1);
// odbc
$db = FactoryAdapter::getInstance("pdo", "odbc:{Driver=IBM i Access ODBC Driver};System=*LOCAL;CCSID=1208;DBQ=qiws;NAM=1", "user", "password");
// sqlsrv
$db = FactoryAdapter::getInstance("pdo", "sqlsrv:Server=localhost\SQLEXPRESS", "user", "password");
// dblib
$db = FactoryAdapter::getInstance("pdo", "dblib:host=localhost;dbname=testdb", "user", "password");
```

Utilisation simple :
```php
<?php

use CFDInnovation\CFD_DB_ADAPTER\FactoryAdapter;

require 'vendor/autoload.php';

try {
// Instanciation du connecteur de base de données
    $db = FactoryAdapter::getInstance("pdo", "host", "user", "password", "qiws", 1);
// Récupération et affichage de tous les clients
    $customers = $db->execRequest('SELECT * FROM qcustcdt', true);
    foreach ($customers as $customer) {
        echo "Nom du client : ".$customer['lstnam']."<br>";
        echo "Ville du client : ".$customer['city']."<br>";
    }
// Récupération et affichage des détails d'un client à partir de son identifiant'
    $cusnum = "938472";
    $customer = $db->prepareRequest('SELECT * FROM qcustcdt WHERE CUSNUM = ?', [$cusnum], true);
    echo "Nom du client : ".$customer[0]['lstnam']."<br>";
    echo "Ville du client : ".$customer[0]['city']."<br>";
    echo "Adresse du client : ".$customer[0]['street']."<br>";
    echo "Code postal du client : ".$customer[0]['zipcod']."<br>";
} catch (Exception | Error $e) {
    var_dump($e);
}
```

Pour récupérer un connecteur :

```php
$db = FactoryAdapter::getInstance(DB_TYPE, DB_HOST, DB_LOGIN, DB_PWD, DB_NAME, 1);
```

Ensuite Il est possible d'envoyer une requête simple via la fonction :

```php
// "QUERY" = requête sans paramètre à executer
// true = booléen indiquant si l'on attend un résultat ou non suite à l'exécution de la requête
$db->execRequest("QUERY", true);
```

Ou une requête préparée :

```php 
// "QUERY" = requête avec paramètres (?) à executer
// Tableau contenant les paramètres ainsi que leurs noms
// true = booléen indiquant si l'on attend un résultat ou non suite à l'exécution de la requête
$db->prepareRequest("QUERY", [$param_value], true);
```

Appel de procédure stockée
```php
// "QUERY" = requête afin d'exécuter la procédure stockée
// Tableau contenant les paramètres ainsi que leurs noms et leurs types
// true = booléen indiquant si l'on attend un résultat ou non suite à l'exécution de la procédure stockée
$result = $db->callProc("call myStoredProc(?, ?)", [['name'=>'id_article', "value"=>'56', 'type'=>'in'], ['name'=>'sortie', "value"=>'', 'type'=>'out']], true)
```

Récupération du dernier ID créé
```php
$lastInsertedId = $db->getLastId();
```

Gestion des transactions
```php
// le booléen n'est utile que sous db2, sinon la commande START TRANSACTION; est exécutée
$db->autoCommit($bool);
// Commit 
$db->commit();
// Rollback
$db->rollback();
```

Récupération du dernier ID enregistré
```php
// Si pas ODBC
$lastId = $db->getLastId();
// Si ODBC
$lastId = $db->getLastId($columnName, $table);
```

### Récupération des informations de la structure d'une base de données

Récupération des informations d'une table
```php
$tableInfos = $db->getTableInfos($tableName);
```

Récupération de la liste des schémas
```php
$tableInfos = $db->getDatabases($dbType);
```
Cette fonction demande en paramètre le type de distribution de base de données si l'on passe par une connexion pdo.
Par exemple si l'ont créé une connexion pdo_odbc, la distribution derrière pourrait être du mysql, db2, sqlsrv, etc...

Les distributions de base de données supportées ainsi que leurs mots-clés :
- Pour PDO 
  - db2
  - sqlsrv
  - dblib


Pour récupérer la liste des schémas d'une base de données db2 par exemple :
```php
$dbType = 'db2';
$tableInfos = $db->getDatabases($dbType);
```

Récupération de la liste des tables d'un schéma
```php
$tableInfos = $db->getTablesFromDatabase(['qiws', 'qwqcent'], 'db2');
```

Récupération de la liste des colonnes d'une table d'un schéma
```php
$tableInfos = $db->getTablesFromDatabase(['qwqcent.orders', 'qwqcent.inventory'], 'db2');
```
Il est très important de préfixer complètement chaque table fournie
