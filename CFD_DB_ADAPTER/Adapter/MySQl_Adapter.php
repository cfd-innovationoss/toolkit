<?php


namespace CFDInnovation\CFD_DB_ADAPTER\Adapter;


use CFDInnovation\Exceptions\not_connected_exception;
use Exception;
use mysqli;
use mysqli_sql_exception;

error_reporting(0);

class MySQl_Adapter extends I_Adapter {
    
    /**
     * @param $hote string (localhost)
     * @param $login string username
     * @param $motpasse string mot de passe
     * @param string|null $dbName Si utilisé, nom de la lib à utiliser
     * @param int $naming
     * @return mysqli
     * Connexion à la base de données
     */
    public function __construct(string $hote, string $login, string $motpasse, string $dbName = null, int $naming = 0, string $type = null)
    {
        
        $this->cnx = mysqli_connect($hote, $login, $motpasse, $dbName);
        if (!$this->cnx) {
            $this->status = "Non connecté";
            throw new mysqli_sql_exception(mysqli_connect_error());
        } else {
            $this->cnx->set_charset('utf8');
            $this->status = "connecté";
        }
    
        $this->type = $type;
        
        return $this->cnx;
        
    }
    
    /**
     * @throws Exception
     * Déconnexion de la base de données
     */
    function __disconnect() {
        if ($this->status == 'connecté')
            mysqli_close($this->cnx);
    }
    
    /**
     * @param $requete string requête à exécuter
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array|bool
     * Exécute une requête préparée avec les paramètres associés
     */
    function prepareRequest(string $requete, array $param, bool $res = null) {
        // Si le statut n'est pas "connect"
        $this->checkConnection();
    
        $save = array();
    
        $type = '';
        $stmtInfos = mysqli_prepare($this->cnx, $requete);
        // Si la préparation à échoué
        if (!$stmtInfos) {
            // On récupère le msg d'erreur et on retourne false
            throw new mysqli_sql_exception(mysqli_error($this->cnx));
        
        }
        // Si $param est un tableau on construit la string de type pour le bind
        $paramMysql = array();
        foreach ($param as $p) {
            $type = $type . strtolower(substr(gettype($p), 0, 1));
            $type = str_replace("n", "i", $type);
            $paramMysql[] = $p;
        }
        mysqli_stmt_bind_param($stmtInfos, $type, ...$paramMysql);
    
        // On Execute et on recup les valeurs dans un tableau
        $resultExec = mysqli_stmt_execute($stmtInfos);
        if (!$resultExec) {
            // Si l'exécution à échoué on récupère le msg d'erreur et on retourne false
            throw new mysqli_sql_exception(mysqli_error($this->cnx));
        }
        if ($res != null) {
            $result = mysqli_stmt_get_result($stmtInfos);
            while ($row = $result->fetch_assoc()) {
                $save[] = $row;
            }
        } else {
            return true;
        }
    
        return $save;
    }
    
    /**
     * @param $requete string requête à exécuter
     * @param $res bool indique si un résultat est attendu
     * @return array
     * Exécute une requête simple sans paramètre
     */
    function execRequest(string $requete, bool $res = true): array {
        $this->checkConnection();
    
        $save = array();
    
        $curseur = mysqli_query($this->cnx, $requete);
        // Si la requête à échoué on récupère l'erreur et retourne false
        if (!$curseur) {
            throw new mysqli_sql_exception(mysqli_error($this->cnx));
        }
        if ($res) {
            while ($row = $curseur->fetch_assoc()) {
                $save[] = $row;
            }
        }
        return $save;
    }
    
    /**
     * @param $requete string requête à exécuter afin d'appeler une procédure stockée
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array
     * Appel une procédure stockée
     */
    function callProc(string $requete, array $param, bool $res = null): ?array {
        return null;
    }
    
    /**
     * @return void
     * Vérification que la connexion est bien établit
     */
    private function checkConnection(): void {
        if (strcmp($this->status, "connecté") != 0) {
            throw new not_connected_exception(" Vous devez d'abord vous connecter.");
        }
    }
    
    /**
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @return array|null
     * Permet de définir le type des paramètres avant l'appel d'une procédure stockée
     */
    function setTypeParamForCallProc(array $param): ?array {
        return null;
    }
    
    function autoCommit($bool) {
        $this->execRequest("START TRANSACTION;", false);
    }
    
    function commit() {
        $this->execRequest("COMMIT;", false);
    }
    
    function rollback() {
        $this->execRequest("ROLLBACK", false);
    }
    
    /**
     * @return int|string
     * retourne le dernier ID crée
     */
    function getLastId() {
        return mysqli_insert_id($this->cnx);
    }
    
    /**
     * @param $table string Nom de la table
     * @return array ['name'=>'article_id', 'type'=> 'int']
     */
    function getTableInfos($table) {
        $this->checkConnection();
        
        $curseur = mysqli_query($this->cnx, "SELECT * FROM $table");
        // Si la requête à échoué on récupère l'erreur et retourne false
        if (!$curseur) {
            throw new mysqli_sql_exception(mysqli_error($this->cnx));
        }
        $data = $curseur->fetch_fields();
        $result = [];
        foreach ($data as $field) {
            $result[] = ['type' => $this->mapFieldType($field->type), 'name' => $field->name];
        }
        return $result;
    }
    
    function mapFieldType($type) {
        switch ($type) {
            case MYSQLI_TYPE_DECIMAL:
            case MYSQLI_TYPE_NEWDECIMAL:
            case MYSQLI_TYPE_FLOAT:
            case MYSQLI_TYPE_DOUBLE:
                return 'float';
            
            case MYSQLI_TYPE_BIT:
            case MYSQLI_TYPE_TINY:
            case MYSQLI_TYPE_SHORT:
            case MYSQLI_TYPE_LONG:
            case MYSQLI_TYPE_LONGLONG:
            case MYSQLI_TYPE_INT24:
            case MYSQLI_TYPE_YEAR:
            case MYSQLI_TYPE_ENUM:
                return 'int';
            
            case MYSQLI_TYPE_TINY_BLOB:
            case MYSQLI_TYPE_MEDIUM_BLOB:
            case MYSQLI_TYPE_LONG_BLOB:
            case MYSQLI_TYPE_BLOB:
            case MYSQLI_TYPE_TIMESTAMP:
            case MYSQLI_TYPE_DATE:
            case MYSQLI_TYPE_TIME:
            case MYSQLI_TYPE_DATETIME:
            case MYSQLI_TYPE_NEWDATE:
            case MYSQLI_TYPE_INTERVAL:
            case MYSQLI_TYPE_SET:
            case MYSQLI_TYPE_VAR_STRING:
            case MYSQLI_TYPE_STRING:
            case MYSQLI_TYPE_CHAR:
            case MYSQLI_TYPE_GEOMETRY:
            default:
                return 'string';
        }
    }
    
    public function getDatabases(string $dbType = 'db2'): array
    {
        $databases = $this->execRequest("SHOW DATABASES");
        $result = [];
        foreach ($databases as $database) {
            $result[] = $database['Database'];
        }
        return $result;
    }
    
    public function getTablesFromDatabase(array $databases, string $dbType = 'db2'): array
    {
        $tableList = [];
        foreach ($databases as $database) {
            $tables = $this->execRequest('SHOW TABLES FROM `'.$database.'`');
            $result = [];
            foreach ($tables as $table) {
                foreach ($table as $key=>$value) {
                    $result[] = ['table_name'=>$value, 'table_text'=>$value, 'table_schema'=>$database];
                }
            }
            $tableList = array_merge($tableList, $result);
        }
       
        return $tableList;
    }
    
    public function getColumnFromTable(array $tables, string $dbType = 'db2'): array
    {
        $result = [];
        foreach ($tables as $table) {
            $tableSql = '`' . explode('.', $table)[0] . '`.`' . explode('.', $table)[1] . '`';
           $columns = $this->execRequest('SHOW COLUMNS FROM '.$tableSql);
           foreach ($columns as $column) {
               $result[] = ['column_name' => $column['Field'],
                            'column_heading' => $column['Field'],
                            'table_name' => explode('.', $table)[1],
                            'table_schema' => explode('.', $table)[0]
               ];
           }
        }
        
        
        
        return $result;
    }
    
}