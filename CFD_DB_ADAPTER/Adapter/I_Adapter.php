<?php


namespace CFDInnovation\CFD_DB_ADAPTER\Adapter;


use Exception;

abstract class I_Adapter
{
    /**
     * @var false|string variable contenant le link de la connexion actuelle
     */
    public $cnx = "";
    /**
     * @var string variable contenant le statut de la connexion ("connecté" ou "Non connecté")
     */
    public $status = "";
    
    /**
     * @var string Type de SGBD
     */
    public $type;
    
    /**
     * @param $hote string (localhost)
     * @param $login string username
     * @param $motpasse string mot de passe
     * @param string|null $dbName Si utilisé, nom de la lib à utiliser
     * @param int $naming
     * @return I_Adapter
     * Connexion à la base de données
     */
    abstract public function __construct(string $hote, string $login, string $motpasse, string $dbName = null, int $naming = 0, string $type = null);

    /**
     * @throws Exception
     * Déconnexion de la base de données
     */
    abstract public function __disconnect();
    
    /**
     * @param $requete string requête à exécuter
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array|bool
     * Exécute une requête préparée avec les paramètres associés
     */
    abstract public function prepareRequest(string $requete, array $param, bool $res = null);
    
    /**
     * @param $requete string requête à exécuter
     * @param $res bool indique si un résultat est attendu
     * @return array
     * Exécute une requête simple sans paramètre
     */
    abstract public function execRequest(string $requete, bool $res = true);
    
    /**
     * @param $requete string requête à exécuter afin d'appeler une procédure stockée
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array
     * Appel une procédure stockée
     */
    abstract public function callProc(string $requete, array $param, bool $res = null);
    
    /**
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @return array
     * Permet de définir le type des paramètres avant l'appel d'une procédure stockée
     */
    abstract public function setTypeParamForCallProc(array $param);

    /**
     * @throws Exception
     */
    public function checkTypesForCallProc($param)
    {
        foreach ($param as $parameter) {
            $parameter['type'] = str_replace(' ', '', $parameter['type']);
            if (strcmp($parameter['type'], 'in') != 0 && strcmp($parameter['type'], 'out') != 0 && strcmp($parameter['type'], 'in/out') != 0) {
                throw new Exception('Un type pour le paramètre ' . $parameter['name'] . ' est attendu (\'in\' ou \'out\'');
            }
        }
    }
    
    abstract public function autoCommit($bool);
    
    abstract public function commit();
    
    abstract public function rollback();
    
    /**
     * @return mixed
     * retourne le dernier ID crée
     */
    abstract public function getLastId();
    
    abstract public function getTableInfos($table);
    
    abstract public function mapFieldType($type);
    
    abstract public function getDatabases(string $dbType = 'db2');
    
    abstract public function getTablesFromDatabase(array $databases, string $dbType = 'db2');
    
    abstract public function getColumnFromTable(array $tables, string $dbType = 'db2');
    
    
}