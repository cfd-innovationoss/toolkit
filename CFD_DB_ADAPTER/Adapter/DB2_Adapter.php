<?php


namespace CFDInnovation\CFD_DB_ADAPTER\Adapter;


use CFDInnovation\Exceptions\db2_sql_exception;
use CFDInnovation\Exceptions\not_connected_exception;
use Exception;

error_reporting(0);

class DB2_Adapter extends I_Adapter {
    
    
    /**
     * @param $hote string (localhost)
     * @param $login string username
     * @param $motpasse string mot de passe
     * @param string|null $dbName Si utilisé, nom de la lib à utiliser
     * @param int $naming
     * @return resource
     * Connexion à la base de données
     */
    public function __construct(string $hote, string $login, string $motpasse, string $dbName = null, int $naming = 0, string $type = null)
    {
        
        if ($naming == 1) {
            $options['i5_naming'] = DB2_I5_NAMING_ON;
            
            if ($dbName != null) {
                $options['i5_libl'] = $dbName;
            }
        } elseif ($dbName != null) {
            $options['i5_lib'] = $dbName;
        }
        
        $options['db2_attr_case'] = DB2_CASE_LOWER;
        $options['i5_dbcs_alloc'] = 1;
        $this->cnx = db2_connect($hote, $login, $motpasse, $options);
        if ($this->cnx === false) {
            $this->status = "Non connecté";
            throw new db2_sql_exception(db2_conn_errormsg());
        } else {
            $this->status = "connecté";
        }
        
        $this->type = $type;
        
        return $this->cnx;
        
        
    }
    
    /**
     * @throws Exception
     * Déconnexion de la base de données
     */
    function __disconnect() {
        if ($this->status == 'connecté')
            db2_close($this->cnx);
        
    }
    
    /**
     * @param $requete string requête à exécuter
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array|bool
     * Exécute une requête préparée avec les paramètres associés
     */
    function prepareRequest(string $requete, array $param, bool $res = null) {
        $this->checkConnection();
    
        $save = array();
    
        $stmtInfos = db2_prepare($this->cnx, $requete);
        // Si la préparation à échoué
        if (!$stmtInfos) {
            // On récupère le msg d'erreur et un retourne false
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
        $nbr = 1;
        foreach ($param as $bind) {
            ${'bind'.$nbr} = $bind;
            db2_bind_param($stmtInfos, $nbr, 'bind'.$nbr, DB2_PARAM_IN);
            $nbr++;
        }
    
        // On Execute et on recup les valeurs dans un tableau
        $resultExec = db2_execute($stmtInfos);
        if (!$resultExec) {
            // Si l'exécution à échoué on récupère le msg d'erreur et on retourne false
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
        if ($res != null) {
            while ($row = db2_fetch_assoc($stmtInfos)) {
                $save[] = $row;
            }
        } else {
            return true;
        }
        
        return $save;
    }
    
    /**
     * @param $requete string requête à exécuter
     * @param $res bool indique si un résultat est attendu
     * @return array
     * Exécute une requête simple sans paramètre
     */
    function execRequest(string $requete, bool $res = true): array {
        $this->checkConnection();
        $save = array();
    
        $curseur = db2_exec($this->cnx, $requete);
        // Si la requête à échoué on récupère l'erreur et retourne false
        if (!$curseur) {
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
        if ($res) {
            while ($row = db2_fetch_assoc($curseur)) {
                $save[] = $row;
            }
        }
        
        return $save;
    }
    
    /**
     * @param $requete string requête à exécuter afin d'appeler une procédure stockée
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array|bool
     * Appel une procédure stockée
     * @throws Exception
     */
    function callProc(string $requete, array $param, bool $res = null) {
        $param = $this->setTypeParamForCallProc($param);
        $this->checkConnection();
    
        $stmtInfos = db2_prepare($this->cnx, $requete);
        // Si la préparation à échoué
        if (!$stmtInfos) {
            // On récupère le msg d'erreur et un retourne false
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
        $nbr = 1;
        foreach ($param as $bind) {
            ${$bind['name']} = $bind['value'];
            db2_bind_param($stmtInfos, $nbr, $bind['name'], $bind['type']);
            $nbr++;
        }
        // On Execute et on recup les valeurs dans un tableau
        $resultExec = db2_execute($stmtInfos);
        if (!$resultExec) {
            // Si l'exécution à échoué on récupère le msg d'erreur et on retourne false
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
        if ($res != null) {
            $save = [];
            for ($i = 0; $i < count($param); $i++) {
                $save[$param[$i]['name']] = ${$param[$i]['name']};
            }
            return $save;
        } else {
            return true;
        }
    }
    
    /**
     * @return void
     * Vérification que la connexion est bien établit
     */
    private function checkConnection(): void {
        if (strcmp($this->status, "connecté") != 0) {
            throw new not_connected_exception(" Vous devez d'abord vous connecter.");
        }
    }
    
    /**
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @return array
     * Permet de définir le type des paramètres avant l'appel d'une procédure stockée
     * @throws Exception
     */
    function setTypeParamForCallProc(array $param): array {
        $this->checkTypesForCallProc($param);
        for ($i=0; $i<count($param); $i++) {
            switch ($param[$i]['type']) {
                case 'in':
                    $param[$i]['type'] = DB2_PARAM_IN;
                    break;
                case 'out':
                    $param[$i]['type'] = DB2_PARAM_OUT;
                    break;
                case 'in/out':
                    $param[$i]['type'] = DB2_PARAM_INOUT;
                    break;
            }
        }
        return $param;
    }
    
    public function autoCommit($bool) {
        $state = ($bool ? DB2_AUTOCOMMIT_ON : DB2_AUTOCOMMIT_OFF);
        if (!db2_autocommit($this->cnx, $state)) {
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
    }
    
    public function commit() {
        if (!db2_commit($this->cnx)) {
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
    }
    
    public function rollback() {
        if (!db2_rollback($this->cnx)) {
            throw new db2_sql_exception(db2_stmt_errormsg());
        }
    }
    
    /**
     * @return string|null
     * retourne le dernier ID crée
     */
    function getLastId(): ?string {
        return db2_last_insert_id($this->cnx);
    }
    
    function getTableInfos($table) {
        $tableInfos = explode('.', $table);
        $result = db2_columns($this->cnx, '', strtoupper($tableInfos[0]), strtoupper($tableInfos[1]), '%');
        while ($row = db2_fetch_assoc($result)) {
            $columns[] = $row;
        }
        $types = [];
        foreach ($columns as $column) {
            $types[] = ['name'=>$column['column_name'], 'type'=>$this->mapFieldType($column['data_type'])];
        }
        return $types;
    }
    
    function mapFieldType($type) {
        switch ($type) {
            case DB2_DOUBLE:
                return 'float';
            
            case DB2_LONG:
                return 'int';
            
            case DB2_CHAR:
            default:
                return 'string';
        }
    }
    function getDatabases(string $dbType = 'db2')
    {
        $sql = "SELECT distinct(table_schema) as name FROM QSYS2.SYSTABLES order by table_schema";
        $result = $this->execRequest($sql);
        $databases = [];
        foreach ($result as $database) {
            $databases[] = $database['name'];
        }
        return $databases;
    }
    
    function getTablesFromDatabase(array $databases, string $dbType = 'db2')
    {
        $nbParam = [];
        $preparedParams = [];
        foreach ($databases as $schema) {
            $nbParam[] = '?';
            $preparedParams[] = strtoupper($schema);
        }
        $sql = "SELECT TABLE_NAME as table_name, TABLE_TEXT as table_text, TABLE_SCHEMA as table_schema FROM QSYS2.SYSTABLES WHERE TABLE_SCHEMA IN (".join(',', $nbParam).") ORDER BY table_type, table_name";
        return $this->prepareRequest($sql, $preparedParams, true);
        
    }
    
    
    function getColumnFromTable(array $tables, string $dbType = 'db2')
    {
        $nbParam = [];
        $preparedParams = [];
        foreach ($tables as $table) {
            $table = explode(".", $table);
            $nbParam[] = "table_schema = ? AND table_name = ?";
            $preparedParams[] = strtoupper($table[0]);
            $preparedParams[] = strtoupper($table[1]);
            
        }
        $sql = "SELECT column_name, column_heading, table_name, table_schema FROM QSYS2.SYSCOLUMNS t WHERE ".join(' OR ', $nbParam);
        return $this->prepareRequest($sql, $preparedParams, true);
        
    }
}