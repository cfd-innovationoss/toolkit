<?php


namespace CFDInnovation\CFD_DB_ADAPTER\Adapter;


use Exception;
use PDO;


class PDO_Adapter extends I_Adapter {
    
    /**
     * @param $hote string (localhost)
     * @param $login string username
     * @param $motpasse string mot de passe
     * @param string|null $dbName Si utilisé, nom de la lib à utiliser
     * @param int $naming
     * @return PDO
     * Connexion à la base de données
     */
    public function __construct(string $hote, string $login, string $motpasse, string $dbName = null, int $naming = 0, string $type = null)
    {
        $this->cnx = new PDO($hote, $login, $motpasse);
        $this->cnx->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
        $this->cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        $this->type = $type;
        
        return $this->cnx;
    }
    
    /**
     * @throws Exception
     * Déconnexion de la base de données
     */
    function __disconnect() {
    
    }
    
    /**
     * @param $requete string requête à exécuter
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array
     * Exécute une requête préparée avec les paramètres associés
     */
    function prepareRequest(string $requete, array $param, bool $res = null) {
        $paramArray = [];
        foreach ($param as $item) {
            $paramArray[] = $item;
        }
        $preparedStatement = $this->cnx->prepare($requete);
        $preparedStatement->execute($paramArray);
        if ($res) {
            $resultArray = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $resultArray = true;
        }
        return $resultArray;
        
    }
    
    /**
     * @param $requete string requête à exécuter
     * @param $res bool indique si un résultat est attendu
     * @return array
     * Exécute une requête simple sans paramètre
     */
    function execRequest(string $requete, bool $res = true) {
        $preparedStatement = $this->cnx->prepare($requete);
        $res = $preparedStatement->execute();
        
        if ($res) {
            $resultArray = $preparedStatement->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $resultArray = true;
        }
        return $resultArray;
    }
    
    /**
     * @param $requete string requête à exécuter afin d'appeler une procédure stockée
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @param $res bool|null indique si un résultat est attendu
     * @return array|bool
     * Appel une procédure stockée
     * @throws Exception
     */
    function callProc(string $requete, array $param, bool $res = null) {
        $param = $this->setTypeParamForCallProc($param);
        $preparedStatement = $this->cnx->prepare($requete);
        
        for ($i = 0; $i < count($param); $i++) {
            $nom = $param[$i]['nom'];
            ${$nom} = $param[$i]['value'];
            $preparedStatement->bindParam($i + 1, $$nom, $param[$i]['type']);
        }
        $preparedStatement->execute();
        if ($res != null) {
            $save = [];
            for ($j = 0; $j < count($param); $j++) {
                $nom = $param[$j]['nom'];
                $save[] = utf8_encode($$nom);
            }
            return $save;
        } else {
            return true;
        }
    }
    
    /**
     * @param $param array tableau associatif contenant les paramètres à inclure dans la requête
     * @return array
     * Permet de définir le type des paramètres avant l'appel d'une procédure stockée
     * @throws Exception
     */
    function setTypeParamForCallProc(array $param): array {
        $this->checkTypesForCallProc($param);
        for ($i = 0; $i < count($param); $i++) {
            switch ($param[$i]['type']) {
                case 'in':
                    $param[$i]['type'] = PDO::PARAM_STR;
                    break;
                case 'out' || 'in/out':
                    $param[$i]['type'] = PDO::PARAM_STR | PDO::PARAM_INPUT_OUTPUT;
                    break;
            }
        }
        return $param;
    }
    
    /**
     * @param $bool
     * @return void
     */
    function autoCommit($bool) {
        $this->cnx->beginTransaction();
    }
    
    /**
     * @return void
     */
    function commit() {
        $this->cnx->commit();
    }
    
    /**
     * @return void
     */
    function rollback() {
        $this->cnx->rollBack();
    }
    
    /**
     * @return false|string
     * retourne le dernier ID crée
     */
    function getLastId($id_name = null, $table = null) {
        if (strcmp($this->type, 'odbc') != 0)
            return $this->cnx->lastInsertId();
        if (!isset($id_name) || !isset($table)) {
            throw new Exception("Si lastInsertId() n'est pas compatible, le nom de la colonne ID ainsi que la table cible sont nécessaires");
        }
        return $this->execRequest("SELECT MAX(".$id_name.") AS ".$id_name." FROM ".$table);
    }
    
    /**
     * @param $table
     * @return array|true
     */
    function getTableInfos($table) {
        $tableInfos = $this->getColumnFromTable([$table]);
        $preparedStatement = $this->cnx->prepare("SELECT * FROM " . $table . " LIMIT 1");
        $res = $preparedStatement->execute();
        
        if ($res) {
            for ($i = 0; $i < count($tableInfos); $i++) {
                $meta = $preparedStatement->getColumnMeta($i);
                $resultArray[$i]['type'] = $this->mapFieldType($meta['native_type']);
                $resultArray[$i]['name'] = $meta['name'];
            }
            
        } else {
            throw new Exception("La table $table n'est pas trouvée");
        }
        return $resultArray;
    }
    
    function mapFieldType($type) {
        switch ($type) {
            case 'LONG':
            case 'TINY':
                return 'int';
            
            case 'DOUBLE':
                return 'float';
            
            case 'VAE_STRING':
            case 'STRING':
            case 'DATE':
            default:
                return 'string';
        }
    }
    
    /**
     * @throws Exception
     */
    function getDatabases(string $dbType = 'db2'): array
    {
        switch ($dbType) {
            case 'db2':
                $sql = "SELECT distinct(table_schema) as name FROM QSYS2.SYSTABLES order by table_schema";
                break;
            case 'sqlsrv':
            case 'dblib':
                $sql = 'SELECT name FROM master.dbo.sysdatabases';
                break;
            default:
                throw new Exception('Le type de base de données n\'est pas supporté');
        }
        $result = $this->execRequest($sql);
        $databases = [];
        foreach ($result as $database) {
            $databases[] = $database['name'];
        }
        return $databases;
    }
    
    /**
     * @param array $databases
     * @param string $dbType
     * @return array
     * @throws Exception
     */
    function getTablesFromDatabase(array $databases, string $dbType='db2'): array
    {
        switch ($dbType) {
            case 'db2':
                $nbParam = [];
                $preparedParams = [];
                foreach ($databases as $schema) {
                    $nbParam[] = '?';
                    $preparedParams[] = strtoupper($schema);
                }
                $sql = "SELECT TABLE_NAME as table_name, TABLE_TEXT as table_text, TABLE_SCHEMA as table_schema FROM QSYS2.SYSTABLES WHERE TABLE_SCHEMA IN (".join(',', $nbParam).") ORDER BY table_type, table_name";
                break;
            case 'sqlsrv':
            case 'dblib':
                $nbParam = [];
                $preparedParams = [];
                foreach ($databases as $schema) {
                    $nbParam[] = '?';
                    $preparedParams[] = strtoupper($schema);
                }
                $sql = "SELECT CONCAT(TABLE_SCHEMA, '.', TABLE_NAME ) as table_name, CONCAT(TABLE_SCHEMA, '.', TABLE_NAME ) as table_text, TABLE_CATALOG as table_schema FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_CATALOG IN (".join(',', $nbParam).") ORDER BY table_type, table_name";
                break;
            default:
                throw new Exception('Le type de base de données n\'est pas supporté');
        }
        
        return  $this->prepareRequest($sql, $preparedParams, true);
        
    }
    
    /**
     * @param array $tables
     * @param string $dbType
     * @return array|true
     * @throws Exception
     */
    function getColumnFromTable(array $tables, string $dbType='db2'): array
    {
        
        switch ($dbType) {
            case 'db2':
                $nbParam = [];
                $preparedParams = [];
                foreach ($tables as $table) {
                    $table = explode(".", $table);
                    $nbParam[] = "table_schema = ? AND table_name = ?";
                    $preparedParams[] = strtoupper($table[0]);
                    $preparedParams[] = strtoupper($table[1]);
                    
                }
                $sql = "SELECT column_name, column_heading, table_name, table_schema FROM QSYS2.SYSCOLUMNS t WHERE ".join(' OR ', $nbParam);
                break;
            case 'sqlsrv':
            case 'dblib':
                $nbParam = [];
                $preparedParams = [];
                foreach ($tables as $table) {
                    $table = explode(".", $table);
                    $nbParam[] = "table_catalog = ? AND table_schema = ? AND table_name = ?";
                    $preparedParams[] = strtoupper($table[0]);
                    $preparedParams[] = strtoupper($table[1]);
                    $preparedParams[] = strtoupper($table[2]);
                    
                }
                $sql = "SELECT column_name, column_name as column_heading, CONCAT(table_schema, '.', table_name) as table_name, table_catalog as table_schema  FROM INFORMATION_SCHEMA.COLUMNS WHERE ".join(' OR ', $nbParam);
                break;
            
            default:
                throw new Exception('Le type de base de données n\'est pas supporté');
        }
        return $this->prepareRequest($sql, $preparedParams, true);
    }
    
}