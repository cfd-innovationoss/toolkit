<?php


namespace CFDInnovation\CFD_DB_ADAPTER;


use CFDInnovation\CFD_DB_ADAPTER\Adapter\DB2_Adapter;
use CFDInnovation\CFD_DB_ADAPTER\Adapter\I_Adapter;
use CFDInnovation\CFD_DB_ADAPTER\Adapter\MySQl_Adapter;
use CFDInnovation\CFD_DB_ADAPTER\Adapter\PDO_Adapter;

class FactoryAdapter {
    
    /**
     * @param $dbType string Type de base de données (mysql, db2 ou connecteur pdo)
     * @param $hote string (localhost)
     * @param $login string username
     * @param $motpasse string mot de passe
     * @param string|null $dbName Si utilisé, nom de la lib à utiliser
     * @param int $naming
     * @return I_Adapter
     */
    public static function getInstance(string $dbType, string $hote, string $login, string $motpasse, string $dbName = null, int $naming = 0): I_Adapter {
        $instance = null;
        
        
        switch ((string) $dbType) {
            case "mysql" :
                $instance = new MySQl_Adapter($hote, $login, $motpasse, $dbName, $naming, $dbType);
                break;
            case "db2" :
                $instance = new DB2_Adapter($hote, $login, $motpasse, $dbName, $naming, $dbType);
                break;
            case "pdo" :
                $pdoType = explode(":", $hote)[0];
                switch ($pdoType) {
                    case "odbc" :
                        $hote = $hote.";DBQ=$dbName;NAMING=$naming";
                        break;
                    case "sqlsrv" :
                        $hote = $hote.";DATABASE=$dbName";
                        break;
                    case "dblib" :
                        $hote = $hote.";dbname=$dbName";
                        break;
                }
                $instance = new PDO_Adapter($hote, $login, $motpasse, $dbName, $naming, $pdoType);
                break;
        }
        return $instance;
        
    }
    
    
}